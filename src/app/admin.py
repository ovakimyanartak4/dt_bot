from django.contrib import admin
from app.internal.models.telegram_user import TelegramUser

admin.site.site_title = "Backend course"
admin.site.site_header = "Backend course"


class TelegramUserAdmin(admin.ModelAdmin):
    list_display = ('user_id', "username", "first_name", "last_name", "phone_number")


admin.site.register(TelegramUser, TelegramUserAdmin)
