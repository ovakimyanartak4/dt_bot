from telegram import Update
from telegram.ext import (
    ContextTypes, CommandHandler,
)

from app.internal.models.telegram_user import TelegramUser
from app.internal.transport.bot.messages import SET_PHONE_MESSAGE


async def me(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    user_id = update.message.from_user.id
    user = TelegramUser.objects.get(user_id=user_id)
    if user.phone_number:
        await update.message.reply_text(f'id: {user.user_id},\n'
                                        f'username: {user.username},\n'
                                        f'phone:{user.phone_number},\n'
                                        f'name: {user.first_name if user.first_name else "unknown"},\n'
                                        f'lastname: {user.last_name if user.last_name else "unknown"}')
    else:
        await update.message.reply_text(SET_PHONE_MESSAGE)

me_handler = CommandHandler("me", me)
