from telegram import Update
from telegram.ext import CallbackContext, CommandHandler

from app.internal.transport.bot.messages import HELP_MESSAGE


async def help(update: Update, context: CallbackContext):
    await update.message.reply_text(HELP_MESSAGE)


help_handler = CommandHandler("help", help)
