GREETINGS_MESSAGE = "Здравсвтуй, %s! \n Для получения информации о возможностях бота напиши /help"
HELP_MESSAGE = """
    /start - Зарегистрироваться в боте
    /set_phone - Указать номер телефона
    /me - Вывести информацию о себе
    /set_fio - Указать имя и фамилию
    """
SET_PHONE_MESSAGE = "Чтобы продолжить укажи номер телефона через /set_phone"
NOT_REGISTERED_MESSAGE = "Чтобы продолжить зарегистрируйся через /start"
DATA_SAVED_MESSAGE = "Данные сохранены"
SHARE_CONTACT = "Поделиться контактом"
SHARE_CONTACT_CONFIRM = "Вы согласны поделиться своей контактной информацией?"
ENTER_FIRST_NAME_MESSAGE = "Введи имя"
ENTER_LAST_NAME_MESSAGE = "Введи фамилию"
