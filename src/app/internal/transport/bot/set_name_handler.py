from telegram import Update
from telegram.ext import CallbackContext, CommandHandler, ConversationHandler, filters, MessageHandler

from app.internal.services.user_service import (
    set_first_name_on_user,
    set_last_name_on_user,
    user_have_phone_number_by_id,
    user_exists_by_id,
)

from .messages import (
    DATA_SAVED_MESSAGE,
    ENTER_FIRST_NAME_MESSAGE,
    ENTER_LAST_NAME_MESSAGE,
    NOT_REGISTERED_MESSAGE,
    SET_PHONE_MESSAGE,
)

FIRST_NAME = 1
LAST_NAME = 2


async def set_name(update: Update, context: CallbackContext):
    user_id = update.effective_user.id
    if not user_exists_by_id(user_id):
        await update.message.reply_text(NOT_REGISTERED_MESSAGE)
        return ConversationHandler.END
    if not user_have_phone_number_by_id(user_id):
        await update.message.reply_text(SET_PHONE_MESSAGE)
        return ConversationHandler.END

    await update.message.reply_text(ENTER_FIRST_NAME_MESSAGE)
    return FIRST_NAME


async def _first_name_handler(update: Update, context: CallbackContext):
    set_first_name_on_user(update.effective_user.id, update.message.text)
    await update.message.reply_text(ENTER_LAST_NAME_MESSAGE)
    return LAST_NAME


async def _last_name_handler(update: Update, context: CallbackContext):
    set_last_name_on_user(update.effective_user.id, update.message.text)
    await update.message.reply_text(DATA_SAVED_MESSAGE)
    return ConversationHandler.END


set_name_handler = ConversationHandler(
    entry_points=[
        CommandHandler("set_name", set_name),
    ],
    states={
        FIRST_NAME: [MessageHandler(filters.ALL, _first_name_handler)],
        LAST_NAME: [MessageHandler(filters.ALL, _last_name_handler)],
    },
    fallbacks=[],
)
