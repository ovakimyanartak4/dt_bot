from telegram import Update
from telegram.ext import (
    ContextTypes, CommandHandler,
)
from app.internal.services.user_service import create_user
from app.internal.transport.bot.messages import GREETINGS_MESSAGE


async def start_command(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    user_id = update.message.from_user.id
    username = update.message.from_user.username
    create_user(user_id, username)
    await update.message.reply_text(GREETINGS_MESSAGE % username)

start_handler = CommandHandler("start", start_command)
