from telegram import Update, ReplyKeyboardRemove, ReplyKeyboardMarkup, KeyboardButton
from telegram.ext import (
    ContextTypes, ConversationHandler, CommandHandler, MessageHandler, filters
)
from app.internal.models.telegram_user import TelegramUser
from app.internal.services.user_service import set_phone_on_user, user_exists_by_id
from app.internal.transport.bot.messages import SHARE_CONTACT, SHARE_CONTACT_CONFIRM, DATA_SAVED_MESSAGE

SET_PHONE = 1


async def get_phone(update: Update, context: ContextTypes.DEFAULT_TYPE):
    con_key = KeyboardButton(SHARE_CONTACT, request_contact=True)
    keyboard_markup = [[con_key]]
    await update.message.reply_text(
        SHARE_CONTACT_CONFIRM,
        reply_markup=ReplyKeyboardMarkup(keyboard_markup)
    )
    return SET_PHONE


async def set_phone(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    phone_number = update.message.contact.phone_number
    user_id = update.message.from_user.id
    username = update.message.from_user.username
    if user_exists_by_id(user_id):
        set_phone_on_user(user_id, phone_number)
    else:
        TelegramUser.objects.create(username=username, user_id=user_id, phone=phone_number)
    await update.message.reply_text(DATA_SAVED_MESSAGE,
                                    reply_markup=ReplyKeyboardRemove())

set_phone_handler = ConversationHandler(
    entry_points=[
        CommandHandler('set_phone', get_phone),
    ],
    states={
        SET_PHONE: [MessageHandler(filters.CONTACT, set_phone)],
    },
    fallbacks=[CommandHandler('set_phone', get_phone)],
)
