import os

import django

from app.internal.transport.bot.help_handler import help_handler
from app.internal.transport.bot.me_handler import me_handler
from app.internal.transport.bot.set_name_handler import set_name_handler
from app.internal.transport.bot.set_phone_handler import get_phone, set_phone
from app.internal.transport.bot.start_handler import start_handler

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'rest.settings')
os.environ["DJANGO_ALLOW_ASYNC_UNSAFE"] = "true"
django.setup()


__all__ = ['set_phone_handler', 'me_handler', 'start_handler', 'set_name_handler', 'help_handler']
