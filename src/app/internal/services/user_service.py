from app.internal.models.telegram_user import TelegramUser


def set_first_name_on_user(user_id: int, first_name: str):
    user_obj = TelegramUser.objects.get(user_id=user_id)
    user_obj.first_name = first_name
    user_obj.save()


def set_last_name_on_user(user_id: int, last_name: str):
    user_obj = TelegramUser.objects.get(user_id=user_id)
    user_obj.last_name = last_name
    user_obj.save()


def user_exists_by_id(user_id: int):
    return TelegramUser.objects.filter(user_id=user_id).exists()


def user_have_phone_number_by_id(user_id: int):
    return bool(TelegramUser.objects.get(user_id=user_id).phone_number)


def create_user(user_id: int, username: str):
    TelegramUser.objects.create(username=username, user_id=user_id)


def set_phone_on_user(user_id: int, phone: str):
    user_obj = TelegramUser.objects.get(user_id=user_id)
    user_obj.phone_number = phone
    user_obj.save()
