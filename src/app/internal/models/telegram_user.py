from django.db import models


class TelegramUser(models.Model):
    user_id = models.IntegerField(default=0)
    username = models.CharField(max_length=100, blank=True, default='')
    first_name = models.CharField(max_length=50, blank=True, verbose_name="First Name")
    last_name = models.CharField(max_length=50, blank=True, verbose_name="Last Name")
    phone_number = models.CharField(max_length=16, blank=True, default='')
