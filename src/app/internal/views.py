from .models.telegram_user import TelegramUser
from django.http import HttpResponse, HttpResponseNotFound


def me(request, user_id):
    user = TelegramUser.objects.filter(user_id=user_id)
    if user.exists():
        user = user.first()
        return HttpResponse(f'id: {user_id}\n, '
                            f'username: {user.username}\n, '
                            f'first_name: {user.first_name},\n'
                            f'last_name: {user.last_name},\n'
                            f' phone_number:{user.phone_number}')
    else:
        return HttpResponseNotFound()
