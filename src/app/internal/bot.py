from telegram.ext import Application, CommandHandler, MessageHandler, filters, ConversationHandler
from app.internal.transport.bot import *
from app.internal.transport.bot.set_phone_handler import set_phone_handler
import os
from dotenv import load_dotenv

load_dotenv()


def start_bot():
    application = Application.builder().token(os.environ.get('TG_TOKEN')).build()
    application.add_handler(set_phone_handler)
    application.add_handler(me_handler)
    application.add_handler(start_handler)
    application.add_handler(set_name_handler)
    application.add_handler(help_handler)
    application.run_polling()
